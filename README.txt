Button commands are as follows:
  escape = pause menu
  space = jump
  e = teleport to last checkpoint
  a & d = movement
  q = save position
  r = jump to saved position
  t = move the world, long timeshift jump



Topics: Platformer, Time travel, cupcakes, parrallel universes
Game name: VHS: Very Horrid Situation
Group name: Time troublers 
Group number: 6
Group members: 
    Casper: caspermc@hotmail.dk
    Kevin: kevin@netivernse.dk
    Niko: nikolaj.ht@gmail.com
    Esteven: esteven.prada@enti.cat
    Alberto: alberto.navarro@enti.cat
Roles:
    Casper Programmer
    Kevin Artist
    Niko Artist
    Esteven Producer
    Alberto Game Designer